package main

import dictionary "gitlab.com/CanICodeDis/wortwand/Dictionary"
import "fmt"
import "log"

import "net"
import "os"

func handleConnection(c net.Conn) {

}

func main() {
	dictionary.DictionaryName = "DN.json"
	arguments := os.Args
	if len(arguments) == 1 {
		fmt.Println("Argumente nicht vergessen")
		return
	}

	Port := ":" + arguments[1]
	listener, err := net.Listen("tcp4", Port)
	if err != nil {
		log.Fatalln(err)
		return
	}
	defer listener.Close()

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println(err)
			return
		}
		go handleConnection(conn)
	}
}
