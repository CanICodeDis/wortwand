package dictionary

import "fmt"
import "sync"
import "encoding/json"
import "io"
import "io/ioutil"
import "os"
import "bufio"
import "log"
import "strings"

type encJSON []byte

type Wpack struct {
	Chinese string
	Pinyin  string
	German  string
	English string
}

var DictionaryName = "dictionary.json"
var rwDicMutex = &sync.Mutex{}
var waitgrp sync.WaitGroup

func ReadNthLine(f string, line int64) (text string, lastLine int64, err error) {
	rwDicMutex.Lock()
	file, erro := os.Open(f)
	if erro != nil {
		log.Println(erro)
		rwDicMutex.Unlock()
		return "", 0, erro
	}
	fileScanner := bufio.NewScanner(file)
	for fileScanner.Scan() {
		lastLine++
		if lastLine == line {
			rwDicMutex.Unlock()
			file.Close()
			return fileScanner.Text(), lastLine, fileScanner.Err()
		}
	}
	file.Close()
	return "", lastLine, io.EOF
}

func CountEntries(f string) int64 {
	rwDicMutex.Lock()
	file, err := os.Open(f)
	defer file.Close()
	if err != nil {
		log.Println(err)
		rwDicMutex.Unlock()
		return 0
	}
	var count int64 = 0
	fileScanner := bufio.NewScanner(file)
	for fileScanner.Scan() {
		count++
	}
	return count
}

func appendEncJson2File(f string, b encJSON) bool {
	rwDicMutex.Lock()
	defer rwDicMutex.Unlock()
	file, err := os.OpenFile(f, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		fmt.Println(err)
		rwDicMutex.Unlock()
		return false
	}
	if _, err := file.Write(b); err != nil {
		fmt.Println(err)
		file.Close()
		rwDicMutex.Unlock()
		return false
	} else {
		file.WriteString("\n")
	}
	file.Close()
	return true
}

func createAndLogNewWpack(ch, pin, en, ge string) []byte {
	entry := CreateNewWpack(ch, pin, en, ge)
	_ = appendEncJson2File(DictionaryName, entry)
	return entry
}

func CreateNewWpack(ch, pin, en, ge string) encJSON {
	pack := &Wpack{ch, pin, ge, en}
	b, err := json.Marshal(pack)
	if err == nil {
		log.Println("Der JSON-Eintrag wurde erfolgreich erstellt.\n")
		return b
	} else {
		log.Println("Der JSON-Eintrag konnte nicht erstellt werden.\n")
		return nil
	}
}

func LoadNthEntry(f string, line int64) (entry Wpack) {

	text, _, _ := ReadNthLine(f, line)
	json.Unmarshal(encJSON(text), &entry)
	return entry
}

func SearchForWord(dictionary, word string) (line int64) {
	rwDicMutex.Lock()
	file, err := os.Open(dictionary)
	if err != nil {
		log.Println(err)
		file.Close()
		rwDicMutex.Unlock()
		return 0
	}
	filescanner := bufio.NewScanner(file)
	line = 1
	for filescanner.Scan() {
		if strings.Contains(filescanner.Text(), word) {
			file.Close()
			rwDicMutex.Unlock()
			return line
		}
		line++
	}
	rwDicMutex.Unlock()
	return 0
}

func replaceLine(dictionary string, line int64, content encJSON) (done bool) {
	rwDicMutex.Lock()
	input, err := ioutil.ReadFile(dictionary)
	if err != nil {
		log.Fatalln(err)
		rwDicMutex.Unlock()
		return false
	}
	lines := strings.Split(string(input), "\n")
	lines[line-1] = string(content)
	output := strings.Join(lines, "\n")
	err = ioutil.WriteFile(dictionary, []byte(output), 0644)
	if err != nil {
		log.Fatalln(err)
		return false
	}
	rwDicMutex.Unlock()
	return true
}
func AddEntry(ch, pin, en, ge string) bool {
	lineNbrCh := SearchForWord(DictionaryName, ch)
	lineNbrPin := SearchForWord(DictionaryName, pin)
	lineNbrEn := SearchForWord(DictionaryName, en)
	lineNbrGe := SearchForWord(DictionaryName, ge)
	alreadyExists := (lineNbrCh == lineNbrPin || lineNbrCh == lineNbrEn || lineNbrCh == lineNbrGe) && ((lineNbrCh | lineNbrPin | lineNbrEn | lineNbrGe) != 0)
	if alreadyExists {
		log.Printf("Der Eintrag existiert bereits in Zeile %v\n", lineNbrCh)
		return false
	} else {
		createAndLogNewWpack(ch, pin, en, ge)
		log.Println("Neuer Eintrag hinzugefügt")
		return true
	}
}
